import os
import torch
import numpy as np
from torch.utils.data.dataset import Dataset
from os import listdir
from os.path import isfile, join
import cv2

class PlantNet(Dataset):
    def __init__(self, root_dir, transform=None):
        """
        Args:
            root_dir (string): Directory with directories with samples.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.transform = transform
        self.samples_dirs = [os.path.join(root_dir, o) for o in os.listdir(root_dir) if os.path.isdir(os.path.join(root_dir,o))]

    def __len__(self):
        return len(self.samples_dirs)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        dir_ = self.samples_dirs[idx]
        img_names = [f for f in listdir(dir_) if (isfile(join(dir_, f)) and f != '.DS_Store' )]
        query_img_idx = list(map(lambda name: name.split('.')[0], img_names)).index('query')
        query_img_name = img_names[query_img_idx]
        img_names.pop(query_img_idx)
        relative_img_names = img_names

        query_img = cv2.imread(join(dir_, query_img_name))
        relative_imgs = [cv2.imread(join(dir_, img_name)) for img_name in relative_img_names]

        query_img = self.resize(query_img)
        relative_imgs = [self.resize(img) for img in relative_imgs]

        sample = {'query': query_img, 'relatives': relative_imgs}

        return sample


    def resize(self, source,  shape = (150, 150)):
        return cv2.resize(source, shape)