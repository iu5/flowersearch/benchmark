from plant_net import PlantNet
import cv2

dataset = PlantNet('./plantNetDataset')

sample = dataset[0]

print("Query", sample['query'].shape)
print("Relative[0]", sample['relatives'][0].shape)

cv2.imshow("query", sample['query'])
cv2.waitKey(0)