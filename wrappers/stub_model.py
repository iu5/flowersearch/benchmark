import torch
import torch.nn as nn
import numpy as np

from wrappers.base_model import BaseModel

class StubModel(BaseModel):
    def __init__(self, d=10):
        self.d = d

    def predict(self, image: np.ndarray) -> np.ndarray:
        return torch.rand(self.d)