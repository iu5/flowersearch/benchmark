from typing import List

import numpy as np


class BaseModel:
    def predict(self, image: np.ndarray) -> np.ndarray:
        """
        Predicts descriptor of shape (D,) for a given image
        """
        raise NotImplementedError()

    def predict_batch(self, images: List[np.ndarray], do_tta: bool = False) -> np.ndarray:
        """
        Predicts descriptor of shape (D,) for a given image
        """
        raise NotImplementedError()