import numpy as np

from datasets.plant_net import PlantNet
from wrappers.stub_model import StubModel
from torch.utils.data import DataLoader

from sklearn.metrics.pairwise import cosine_similarity

from metrics.precision_at_k import precision_at_k
from metrics.ndcg_score import ndcg_score
from metrics.r_precision import r_precision

import nmslib

def collect_emds(dataset, model):
    data_loader = DataLoader(dataset=dataset, shuffle=False)
    queries_embs = []
    relatives_embs = []
    all_embs = []
    all_labels = []
    for i, sample in enumerate(data_loader):
        query = sample['query']
        relatives = sample['relatives']

        query_pred = model.predict(query)
        queries_embs.append(query_pred)
        all_embs.append(query_pred)
        all_labels.append(i)
        for relative in relatives:
            rel_pred = model.predict(relative)
            relatives_embs.append(rel_pred)
            all_embs.append(rel_pred)
            all_labels.append(i)

    all_embs = np.stack(all_embs, axis=0)
    queries_embs = np.stack(queries_embs, axis=0)
    relatives_embs = np.stack(relatives_embs, axis=0)
    all_labels = np.asarray(all_labels)

    return {"queries": queries_embs, "relatives": relatives_embs, "all": all_embs, "labels": all_labels}

def sorting_function(x, origin, compare_function = cosine_similarity):
    return compare_function(x, origin)

def sort_by_query(query, sortable, compare_function = cosine_similarity):
    sorted = sorted(sortable, lambda x: sorting_function(x, query, compare_function),reverse=True)
    return sorted

def map_labels(item, true):
    if (item == true):
        return 1
    return 0

def main():
    dataset = PlantNet('./datasets/plantNetDataset')
    model = StubModel()

    search_db = collect_emds(dataset, model)

    index = nmslib.init(method='hnsw', space='cosinesimil')
    index.addDataPointBatch(search_db["all"])
    index.createIndex(print_progress=True)

    length = search_db["all"].shape[0]

    metrics = {'r-precision': []}

    for query_idx in range(search_db["queries"].shape[0]):

        ids, distances = index.knnQuery(search_db["queries"][query_idx], k=length)

        # print(length, ids.shape)

        result_labels = search_db["labels"][ids]

        result = list(map(lambda id: map_labels(id, query_idx), result_labels))
        metrics["r-precision"].append(r_precision(result))

    metrics["average-r-precision"] = sum(metrics["r-precision"]) / len(metrics["r-precision"])

    print("Average R-Precision: ", metrics["average-r-precision"])

if __name__ == 'main':
    main()