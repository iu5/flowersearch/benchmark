import tensorflow as tf

def precision_at_k(y_true, y_score, k):
    """
    :param y_true: int64 Tensor or SparseTensor with shape [D1, ... DN, num_labels] or [D1, ... DN],
    :param y_score: Float Tensor with shape [D1, ... DN, num_classes] where N >= 1.
    :param k: Integer, k for @k metric.
    :return: Scalar float64 Tensor with the value of true_positives divided by the sum of true_positives and false_positives.
    """
    # some prepros
    return tf.compat.v1.metrics.precision_at_k(y_true, y_score, k, class_id=None, weights=None, metrics_collections=None,updates_collections=None, name=None)