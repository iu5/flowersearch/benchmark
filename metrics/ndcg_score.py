import sklearn

def ndcg_score(y_true, y_score, k=None):
    """
    :param y_true: ndarray of shape (n_samples, n_labels)
    :param y_score: ndarray of shape (n_samples, n_labels)
    :param k: int or None
    :return: float
    """
    return sklearn.metrics.ndcg_score(y_true, y_score, k=None, sample_weight=None, ignore_ties=False)